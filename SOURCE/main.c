/******************************************************************************
 * @file [ main.c ]                                                           *
 *                                                                            *
 *  @author   Ossiel Vazquez Ruiz                         * * * * * * * * * * *
 *     @date   05/08/2019                                 *                   *
 *                                                        *  @version   0.01  *
 *  @brief                                                *                   *
 *    main program for Wdog                               *                   *
 * ************************************************************************** *
 *  @details                                                                  *
 *    $ program developed for EFM32 zero gecko card powerby .. SiliconLabs    *
 *    $ Modules used: GPIO, CMU & WDOG                                        *
 * ************************************************************************** *
 *  @warning                             *                                    *
 *    program carried out as a TEST      *                                    *
 *                                       *   @copyright  Copyright (c) 2019   *
 *  @bug                                 *                                    *
 *    El cielo es el limite              *                                    *
******************************************************************************/

/*** INCLUDE HEADER FILE ***/
#include "em_device.h"
#include "em_system.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_rmu.h"
#include "em_gpio.h"
#include "em_wdog.h"
#include "wdog_cdsi.h"
#include "toggleLed_cdsi.h"
#include "efm32zg_reset_cause.h"

/*** MACRO DEFINITIONS ***/

/*** GLOBAL VARIABLES ***/
uint16_t cntst = 0;
bool wdgrst = false;


/*** PROTOTYPE DEFINITIONS ***/

/*** FUNCTION PROTOTYPES DECLARATIONS ***/


/**************************************************************************//**
 * @brief
 *    Function main()
 *
 * @details
 *    The main name function is the main and first function to execute
 *
 * @param[in]
 *    NONE
 *
 * @note
 *    Module configuration functions are invoked at the beginning of the main
 *
 * @return 
 *    NULL is always returned
******************************************************************************/
int main(void)
{
    // Chip errata 
    CHIP_Init();

#ifdef _T2GLD2S_H_ /** only toggleLed_cdsi.h**/
    // Configure the Push Buttons and the LEDs 
    initGPIO_tgg();
#endif
    wdgrst = watchdog_getreset_cdsi();
	uint32_t reset_flags = RMU_ResetCauseGet();
		
	// Clear Reset causes so we know which reset occurs the next time 
    RMU_ResetCauseClear();
	print_reset_cause(reset_flags); //OTHER RESET
	
	while(wdgrst)
	{
		led_toggle(TOGGLE_LED1);
		
		watchdogfeed_cdsi(); //KICK DOG
		
		 while(!(GPIO_PinInGet(PORTC,PB0_PIN)))
		  {
			// Enter EM3 while the button is pressed
			// (WDOG will continue running)
			watchdogenable_cdsi(false); // enable - disable WDOG
			led_toggle(TOGGLE_OFF);
		  }
	}
	
	watchdoginit_cdsi(); //START COUNT WDOG TIMMER

    while (1)   /** ONLY TEST **/
    {
        // Configure and Initialize the Watchdog timer 
        // Feed the watchdog 
	  	led_toggle(TOGGLE_LED0);
        watchdogfeed_cdsi(); //KICK DOG
		
        // Do not feed the WDOG if PB0 is pressed 
	  while(!(GPIO_PinInGet(PORTC,PB0_PIN)))
	  {
		// Enter EM3 while the button is pressed
		// (WDOG will continue running)
		EMU_EnterEM3(true);
	  }

    }
} /** main() **/

/*** END OF FILE ***/