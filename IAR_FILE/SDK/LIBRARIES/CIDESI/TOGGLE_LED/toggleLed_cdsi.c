/******************************************************************************
 * @file toggled_cdsi.c                                                       *
 *                                                                            *
 *  @author   Ossiel Vazquez Ruiz                                             *
 *     @date   11/09/2019                                                     *
 *                                                            @version   0.01 *
 *  @brief                                                                    *
 *    File C for toggled led                                                  *
 *                                                                            *
 *  @details                                                                  *
 *    $ program developed for efm32 zero gecko card powerby .. SiliconLabs    *
 *    $ Modules used: gpio                                                    *
 *                                                                            *
 *  @warning                                                                  *
 *    program carried out as a test                                           *
 *                                            @copyright  Copyright (c) 2019  *
 *  @bug                                                                      *
 *    El cielo es el limite                                                   *
******************************************************************************/
/*** Includes Header ***/
#include "toggleLed_cdsi.h"


/*** GLOBAL VARIABLES ***/
uint32_t cont1 = 0, cont2 = 0, cont3 = 0, cont4 = 0;


/**************************************************************************//**
 * @brief
 *    GPIO initialization
 *
 * @details
 *    The main name function is the main and first function to execute
 *
 * @param[in]
 *    NONE
 *
 * @note
 *    Module configuration functions are invoked at the beginning of the main
 *
 * @return 
 *    NULL is always returned
******************************************************************************/
void initGPIO_tgg(void)
{
  #ifndef _gpio_conf_std_
  // Enable clock for the GPIO module 
  CMU_ClockEnable(cmuClock_GPIO, true);

  // CONFIGURACION DEL PUERTO C
  GPIO_DriveModeSet(PORTC, gpioDriveModeStandard); 
  #endif

  //CONFIGURACION DE LOS PINES DEL PUERTO C PARA LOS LEDS
  GPIO_PinModeSet(PORTC, LED0_PIN, gpioModePushPullDrive, 0);
  GPIO_PinModeSet(PORTC, LED1_PIN, gpioModePushPullDrive, 0);

#if 1
  // Configure PB0 as input  
  GPIO_PinModeSet(PORTC, PB0_PIN, gpioModeInputPull, 1);
#endif
  
} /** initGPIO() **/


/******************************************************************************
 * @brief 
 *    Function to toggled leds.
 *
 * @details
 *    The LEDs change state and make a shift each time you enter the function.
 *
 * @param[in]
 *    NONE
 * 
 * @note
 *    None
 * 
 * @return 
 *    NULL is always returned
******************************************************************************/
uint16_t led_toggle(uint16_t ldtg)
{
  #ifdef _wdg_cds_
  WDOG_Feed(); //kick dog
  #endif

  bool a = false;
  bool b = false;
  bool c = false;
  bool d = false;
  bool e = false;
	
  if (ldtg == TOGGLE_LED0_LED1)
  {
    a = true;
  } 
    else if (ldtg == TOGGLE_LED0)
    {
      b = true;
    }   else if (ldtg == TOGGLE_LED1)
        {
        c = true;
        }   else if (ldtg == TOGGLE_ALL)
            {
            	d = true;
            }
  			else if (ldtg == TOGGLE_OFF)
            {
            	e = true;
            }
  else
  {
    GPIO_PinOutClear(PORTC, LED0_PIN);
    GPIO_PinOutClear(PORTC, LED1_PIN);
  }

  /** Togglea entre los dos leds **/
  while (a)
  {
    cont1 ++;

    if ( (cont1 % 2) != 0 )
    {
      GPIO_PinOutSet(PORTC, LED1_PIN);
      GPIO_PinOutClear(PORTC, LED0_PIN);
    }  else {   
              GPIO_PinOutSet(PORTC, LED0_PIN);
              GPIO_PinOutClear(PORTC, LED1_PIN);
            }
	for(int i = 0; i < 30000; ++i)
		{WDOG_Feed();}
    a = false;
  }

  /** Togglea led 0 **/
  while (b)
  {

    cont2 ++;

    if ( (cont2 % 2) != 0 )
    {
      GPIO_PinOutSet(PORTC, LED0_PIN);
    }  else {   
              GPIO_PinOutClear(PORTC, LED0_PIN);
            }
	for(int i = 0; i < 30000; ++i)
		{WDOG_Feed();}
    b = false;
  }

  /** Togglea led 1 **/
  while (c)
  {
    cont3 ++;
 
    if ( (cont3 % 2) != 0 )
    {
      GPIO_PinOutSet(PORTC, LED1_PIN);
    }  else {   
              GPIO_PinOutClear(PORTC, LED1_PIN);
            }
	for(int i = 0; i < 30000; ++i)
		{WDOG_Feed();}
    c = false;
  }
   
   /* toggle both led*/
   while (d)
   {
    cont4 ++;

    if ( (cont4 % 2) != 0 )
    {
      GPIO_PinOutSet(PORTC, LED1_PIN);
      GPIO_PinOutSet(PORTC, LED0_PIN);
    }  else {   
              GPIO_PinOutClear(PORTC, LED0_PIN);
              GPIO_PinOutClear(PORTC, LED1_PIN);
            }
	for(int i = 0; i < 30000; ++i)
		{WDOG_Feed();}
    d = false;
  }
  
 	while (e)
	{
	  GPIO_PinOutClear(PORTC, LED0_PIN);
	  GPIO_PinOutClear(PORTC, LED1_PIN);
	}

  return 0;
} /** led_toggle() **/

/** END OF FILE **/