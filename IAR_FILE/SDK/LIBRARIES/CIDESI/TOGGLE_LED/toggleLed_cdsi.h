/******************************************************************************
 * @file toggled_cdsi.h
 * 
 *  @author   Ossiel Vazquez Ruiz
 *     @date   11/09/2019
 *                                                             @version   0.01
 *  @brief
 *    Header file H for toggled led
 * 
 *  @details
 *    $ program developed for efm32 zero gecko card powerby ... SiliconLabs
 *    $ Modules used: gpio
 * 
 *  @warning
 *    program carried out as a test
 *                                              @copyright  Copyright (c) 2019
******************************************************************************/
#ifndef _T2GLD2S_H_
#define _T2GLD2S_H_

/** INCLUDE HEADER FILE **/
#include "em_device.h"
#include "em_system.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "wdog_cdsi.h"

/*** MACRO DEFINITIONS ***/
#define PORTC    gpioPortC
#define LED0_PIN    10
#define LED1_PIN    11
#define PB0_PIN     8
#define TOGGLE_LED0_LED1 0x0001
#define TOGGLE_LED0 0x002
#define TOGGLE_LED1 0x003
#define TOGGLE_ALL 0x004 
#define TOGGLE_OFF 0x005

#ifndef _wdg_cds_
#   define _wdg_cds_
#endif


/*** FUNCTION PROTOTYPES DECLARATIONS ***/
extern uint16_t led_toggle(uint16_t ldtg);
extern void initGPIO_tgg(void);

#endif /** _T2GLD2S_H_ **/

/*** END OF FILE ***/