/******************************************************************************
 * @ file efm32zg_reset_cause.c
 *
 *  Created on: 11/03/2019
 *      Author: AntoMota
 ******************************************************************************/

/* Headers */
#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_rmu.h"
#include "toggleLed_cdsi.h"
/* Definitions */
#define SUCCESS 0

/* Functions */

/***************************************************************************//**
 * @brief
 *   Gets the Reset Cause Flags mask.
 *
 * @details
 *   Using EMlib fucntions, the Reset Cause Flags Mask is obtained, so its possi
 *      ble to determine the previous reset cause.
 *
 * @note
 *   None.
 *
 * @param[in] reset_flags
 *   A variable to store the current REset Flags Mask.
 *
 * @return
 *   0, if SUCCESS.
 ******************************************************************************/
uint32_t get_reset_cause()
{
    uint32_t reset_flags = RMU_ResetCauseGet();
    return reset_flags;
}


/***************************************************************************//**
 * @brief
 *   Prints the previously obtained reset mask through USART 0.
 *
 * @details
 *   After obtaining the Reset cause Flags Mask, you can print the results for
 *      for debugging purposes.
 *
 * @note
 *   Ensure to pass a valid value.
 *
 * @param[in] reset_flags
 *   The vairable containing the resulting flag mask
 *
 * @return
 *   0, if SUCCESS.
 *******    ***********************************************************************/
uint8_t print_reset_cause(uint32_t reset_flags)
{

    if (reset_flags & RMU_RSTCAUSE_PORST){
       // uartPutData0("Power On Rst\r\n", 16);
    }

  //Brown Out Detector Unregulated Domain= BOD UD
    if (reset_flags & RMU_RSTCAUSE_BODUNREGRST){
        //uartPutData0("BOD UD Reset\r\n", 16);
    }

    //Brown Out Detector Regulated Domain= BOD RD
    if (reset_flags & RMU_RSTCAUSE_BODREGRST){
       // uartPutData0("BOD RD Reset\r\n", 16);
#if 0
    bool conti = 1;
       while (conti)
       {
           for (int i = 0; i < 9; i++)
           {
                led_toggle(TOGGLE_LED0);
                
           }
           led_toggle(TOGGLE_OFF);
           for (int i = 0; i < 9; i++)
           {
                led_toggle(TOGGLE_LED1);
                
           }
           led_toggle(TOGGLE_OFF);
            for (int i = 0; i < 4; i++)
           {
                led_toggle(TOGGLE_ALL);
                
           }
          led_toggle(TOGGLE_OFF);
		  
            conti= 0;
       }   
#endif
    }
    //External pin reset
    if (reset_flags & RMU_RSTCAUSE_EXTRST)
	{
        //uartPutData0("Ext Pin Reset\r\n", 17);
	  	bool sts = 1;
		while(sts)
		{
			led_toggle(TOGGLE_LED0_LED1);
			watchdogfeed_cdsi();
			
			while(!(GPIO_PinInGet(PORTC,PB0_PIN)))
			{
			  // Enter EM3 while the button is pressed
			  // (WDOG will continue running)
			  led_toggle(TOGGLE_OFF);
			  EMU_EnterEM3(true);
			}
		}
    }
  /*  if (reset_flags & RMU_RSTCAUSE_WDOGRST){
        //uartPutData0("Watchdog Reset\r\n", 18);
    } */
    if (reset_flags & RMU_RSTCAUSE_LOCKUPRST){
        //uartPutData0("LOCKUP Reset\r\n", 16);
        led_toggle(TOGGLE_ALL);
    }
    //System Request Reset
    if (reset_flags & RMU_RSTCAUSE_SYSREQRST){
		//uartPutData0("Syst Req Reset\r\n", 18);
    }
    if (reset_flags & RMU_RSTCAUSE_EM4RST){
       // uartPutData0("EM4 Reset\r\n", 13);
    }
    if (reset_flags & RMU_RSTCAUSE_EM4WURST){
        //uartPutData0("EM4 Wake-up Rst\r\n", 19);
    }
    if (reset_flags & RMU_RSTCAUSE_BODAVDD0){
        //uartPutData0("AVDD0 Bod Rst\r\n", 17);
    }
    if (reset_flags & RMU_RSTCAUSE_BODAVDD1){
        //uartPutData0("AVDD1 Bod Rst\r\n", 17);
    }

    // Clear reset causes so we know which reset matters next time around
    RMU_ResetCauseClear();

    return SUCCESS;
}
