/******************************************************************************
 * @file  wdog_cdsi.h
 * 
 *  @author   Ossiel Vazquez Ruiz
 *     @date   10/092019
 *                                                             @version   0.01
 *  @brief
 *    Header file H for the WDOG module library
 * 
 *  @details
 *    $ program developed for efm32 zero gecko card powerby ... SiliconLabs
 *    $ Modules used: gpio, interruptions, systick and wdog
 * 
 *  @warning
 *    program carried out as a test
 *                                              @copyright  Copyright (c) 2019
******************************************************************************/

#ifndef __WDGCDS_H__
#define __WDGCDS_H__

/** INCLUDE HEADER FILE **/
#include "em_device.h"
#include "em_system.h"
#include "em_chip.h"
#include "em_rmu.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_gpio.h"
#include "em_wdog.h"

/** MACRO DEFINITIONS **/


/** PROTOTYPE DEFINITIONS **/


/** FUNCTION PROTOTYPES DECLARATIONS **/
extern void watchdoginit_cdsi(void);
extern bool watchdog_getreset_cdsi(void);
extern void watchdogfeed_cdsi(void);
extern void watchdogenable_cdsi(bool);

#endif /* __WDGCDS_H_ */

/*** END OF FILE ***/